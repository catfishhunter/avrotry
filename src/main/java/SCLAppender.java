import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.core.AppenderBase;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class SCLAppender extends AppenderBase<LoggingEvent> {

        private ConcurrentMap<String, LoggingEvent> eventMap
                = new ConcurrentHashMap<>();

        @Override
        protected void append(LoggingEvent event) {
            eventMap.put(Long.toString(System.currentTimeMillis()), event);
        }

        public Map<String, LoggingEvent> getEventMap() {
            return eventMap;
        }
}

