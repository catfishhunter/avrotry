package ru.myTry;

import org.apache.avro.file.DataFileWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecordBase;
import org.apache.kafka.common.serialization.Serializer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

public class KafkaAvroListSerializer<T extends SpecificRecordBase> implements Serializer<List<T>> {
    private String encoding = "UTF8";

    @Override
    public byte[] serialize(String topic, List<T> payload) {
        byte[] bytes = null;
        if (payload != null) {
            try(DataFileWriter dataFileWriter = new DataFileWriter<T>( new SpecificDatumWriter(payload.get(0).getSchema()))){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            //DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<>(payload.getSchema());
            dataFileWriter.create(payload.get(0).getSchema(), byteArrayOutputStream);
            for(T item : payload)
                dataFileWriter.append(item);
            dataFileWriter.flush();
            bytes = byteArrayOutputStream.toByteArray();
        }
        catch (IOException e) {e.printStackTrace();}
        }
        return bytes;
    }

}
