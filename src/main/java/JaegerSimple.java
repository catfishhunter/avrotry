import io.jaegertracing.Configuration;
import io.opentracing.Scope;
import io.opentracing.Span;
import io.opentracing.Tracer;
import io.opentracing.tag.Tags;
import io.opentracing.util.GlobalTracer;

import java.time.Duration;
import java.util.Hashtable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class JaegerSimple {
    public static void main(String[] args) throws InterruptedException {

       // Tracer tracer = GlobalTracer.get();
        Configuration config = Configuration.fromEnv()
                //.withTraceId128Bit(true)
                .withServiceName("Adapter");

        Tracer tracer =config.getTracer();
        for (int i = 0; i < 10; i++) {
            Span span1 = tracer.buildSpan("Smev3Adapter").withTag("mode","distributedTrace").start();
            Scope scope = tracer.scopeManager().activate(span1);
            try
            {
                span1.setTag("VIN","JP123");
                Tags.SPAN_KIND.set(span1,"qqq");

                TimeUnit.MILLISECONDS.sleep(10);


                Span span2 = tracer.buildSpan("Pebble")/*.asChildOf(span1)*/.start();
                Scope scope1 = tracer.scopeManager().activate(span2);
                TimeUnit.MILLISECONDS.sleep(3);
                Span span3 = tracer.buildSpan("Prostore")/*.asChildOf(span2)*/.start();
                Scope scope2 = tracer.scopeManager().activate(span3);
                TimeUnit.MILLISECONDS.sleep(50);
                scope2.close();
                span3.finish();
                scope1.close();
                span2.finish();
                TimeUnit.MILLISECONDS.sleep(22);

                Span span4 = tracer.buildSpan("response").start();
                Scope scope4 =tracer.scopeManager().activate(span4);
                TimeUnit.MILLISECONDS.sleep(10);
                scope4.close();
                span4.finish();

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Span span5 = tracer.buildSpan("ackThread").asChildOf(span1).start();
                        Scope scope5=tracer.scopeManager().activate(span5);
                        try {
                            TimeUnit.MILLISECONDS.sleep(125);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        scope5.close();
                        span5.finish();
                    }
                }).start();


               /* Span span6 = tracer.buildSpan("some").start();
                tracer.scopeManager().activate(span6);
                TimeUnit.MILLISECONDS.sleep(3);
                span6.finish();*/
                TimeUnit.MILLISECONDS.sleep(1);
                System.out.println(i);
            } finally {
                scope.close();
                span1.finish();
            }

        }
    }
}
