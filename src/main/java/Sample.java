import example.avro.Cat;
import example.avro.User;
import org.apache.avro.file.DataFileReader;
import org.apache.avro.file.DataFileWriter;
import org.apache.avro.io.DatumReader;
import org.apache.avro.io.DatumWriter;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;

import org.apache.avro.specific.SpecificRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;


public class Sample {


    private static   Producer<String, SpecificRecord> getPproducer() {
        Properties properties = new Properties();

        //properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "p2-ads-04.load.local:9092");
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "192.168.5.123:9092");
        //properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "t5-ads-01.ru-central1.internal:9092");


        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer.class);

        Producer<String, SpecificRecord> producer = new KafkaProducer<>(properties);
        return producer;

    }

    private static   Producer<String, String> getPproducerString() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "p1-adsk-01:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

        Producer<String, String> producer = new KafkaProducer<>(properties);
        return producer;

    }

    public static void main(String[] args) {
        Cat cat1= new Cat(15L,18L,"Tom",0);
        Cat cat2= new Cat(16L,258L,"Charli",0);
        Cat cat3= new Cat(17L,22L,"Barsik",0);
        List<Cat> cats=Arrays.asList(cat1,cat2,cat3);
        cats.stream().forEach(Sample::publishKafka);

       /* User user1 = new User("Ivan", 15L, "Blue");
        User user2 = new User("Ben", 7L, "red");
        User user3 = new User("Flakon", 2L, "green");

        List<User> users = Arrays.asList(user1, user2, user3);
        users.stream().forEach(Sample::publishKafka);*/

        DatumWriter<Cat> myDatumWriter = new SpecificDatumWriter<Cat>(cat1.getSchema());
        DataFileWriter<Cat> dataFileWriter = new DataFileWriter<Cat>(myDatumWriter);
        try {
            dataFileWriter.create(cat1.getSchema(), new File("cats.avro"));
            cats.stream().forEach(cat -> {
                try {
                    dataFileWriter.append(cat);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
            dataFileWriter.close();

        DatumReader<Cat> myDatumReader = new SpecificDatumReader<Cat>(Cat.class);
        DataFileReader<Cat> dataFileReader = new DataFileReader<Cat>(new File("cats.avro"), myDatumReader);
        Cat cat = null;
        while (dataFileReader.hasNext()) {
            cat = dataFileReader.next(cat);
            System.out.println(cat);
        }
        } catch (IOException e) {
            e.printStackTrace();
        }
        try{
            System.out.println();
        }
        finally {
            System.out.println();
        }


    }

    private static void publishKafka(User u) {
        Producer<String, SpecificRecord> prod = getPproducer();
        prod.send(new ProducerRecord<>("1users", u.getName(), u));
        prod.flush();
    }

    private static void publishKafka(Cat u) {
        Producer<String, SpecificRecord> prod = getPproducer();
        prod.send(new ProducerRecord("3cats", Long.toString(u.getId()),u));
        prod.flush();
    }
}
