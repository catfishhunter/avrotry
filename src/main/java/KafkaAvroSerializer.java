import org.apache.avro.file.DataFileWriter;
import org.apache.avro.specific.SpecificDatumWriter;
import org.apache.avro.specific.SpecificRecordBase;
import org.apache.kafka.common.serialization.Serializer;
import java.io.*;

public class KafkaAvroSerializer<T extends SpecificRecordBase> implements Serializer<T> {
    private String encoding = "UTF8";

    @Override
    public byte[] serialize(String topic, T payload) {
        byte[] bytes = null;
        if (payload != null) {
            try(DataFileWriter dataFileWriter = new DataFileWriter<T>( new SpecificDatumWriter(payload.getSchema()))){
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            //DatumWriter<GenericRecord> datumWriter = new GenericDatumWriter<>(payload.getSchema());
            dataFileWriter.create(payload.getSchema(), byteArrayOutputStream);
            dataFileWriter.append(payload);
            dataFileWriter.flush();
            bytes = byteArrayOutputStream.toByteArray();
        }
        catch (IOException e) {e.printStackTrace();}
        }
        return bytes;
    }

}
