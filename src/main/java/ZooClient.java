import example.avro.CatLong;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
/*import ru.itone.dtm.zookeeper.ds.configuration.ZookeeperProperties;
import ru.itone.dtm.zookeeper.ds.connection.ThrowableConsumer;
import ru.itone.dtm.zookeeper.ds.connection.ThrowableFunction;
import ru.itone.dtm.zookeeper.ds.connection.ZookeeperExecutor;
import ru.itone.dtm.zookeeper.ds.connection.impl.ZookeeperConnectionProviderImpl;
import ru.itone.dtm.zookeeper.ds.connection.impl.ZookeeperExecutorImpl;
import ru.itone.dtm.zookeeper.ds.dao.AbstractZookeeperDao;
import ru.itone.dtm.zookeeper.ds.dto.VersioningEntity;*/

import java.util.List;

/*import ru.itone.dtm.zookeeper.ds.configuration.ZookeeperProperties;
import ru.itone.dtm.zookeeper.ds.connection.ZookeeperConnectionProvider;
import ru.itone.dtm.zookeeper.ds.connection.ZookeeperExecutor;
import ru.itone.dtm.zookeeper.ds.connection.impl.ZookeeperConnectionProviderImpl;
import ru.itone.dtm.zookeeper.ds.connection.impl.ZookeeperExecutorImpl;
*/
public class ZooClient {
  /*  public static void main(String[] args) {
        //ZookeeperConfiguration zc = new ZookeeperConfiguration();
        ZookeeperProperties zp=new ZookeeperProperties();
        zp.setConnectionString("localhost");
        zp.setConnectionTimeoutMs(86400000L);
        zp.setSessionTimeoutMs(30000L);
        zp.setChroot("/a");
        String envName = "envb";
        ZookeeperConnectionProviderImpl cpi = new ZookeeperConnectionProviderImpl(zp, envName);
        Vertx vertx = Vertx.vertx();
        ZookeeperExecutorImpl executor = new ZookeeperExecutorImpl(cpi, vertx);
        EntityZoo ez = new EntityZoo(5L, "qaz");
        //executor.exists("/a").onComplete(e-> System.out.println("existing="+e));

        MyZooDao mzd = new MyZooDao(executor, "/q1");
        mzd.create(ez);

    }
    public static class EntityZoo extends VersioningEntity{
        Long id;
        String name;

        public EntityZoo(Long id, String name) {
            this.id = id;
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

   public static class MyZooDao extends AbstractZookeeperDao<String, EntityZoo> {
        protected MyZooDao(ZookeeperExecutor executor, String envPath) {
            super(executor, envPath);
        }

        @Override
        public Future<Void> create(EntityZoo entityZoo) {
            return super.create(entityZoo, entityZoo.getName());

        }

        @Override
        public Future<Void> update(EntityZoo entityZoo) {
            return super.update(entityZoo, entityZoo.getName());
        }

        @Override
        public Future<Void> updateForcibly(EntityZoo entityZoo) {
            return super.updateForcibly(entityZoo, entityZoo.getName());
        }
    }*/
}
