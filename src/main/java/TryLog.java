import org.slf4j.*;

import java.time.Instant;

public class TryLog {
    static Logger logger=LoggerFactory.getLogger(TryLog.class);
    public static void main(String[] args) {
        logger.info("event1");
        MDC.put("messageID","mess567");
        MDC.put("requestId","req2");
        logger.info(MarkerFactory.getMarker("TYPE_SCL"),"{} {} {} {} {} {}","podd-mppr","request received","123",Instant.now().toEpochMilli(),"","");
        MDC.clear();
        logger.info("event2");

    }

    static class Event{
        public Event(String system, String resultCode, String requestId, Long timestampUTC, String message, String description) {
            this.system = system;
            this.resultCode = resultCode;
            this.requestId = requestId;
            this.message = message;
            this.description = description;
            this.timestampUTC = timestampUTC;
        }

        String system;
        String resultCode;
        String requestId;
        String message;
        String description;
        Long timestampUTC;
    }

}
