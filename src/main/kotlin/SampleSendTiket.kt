import datamart.query.QueryRequest
import example.avro.Cat
//import jdk.nashorn.internal.objects.NativeArray
import kotlinx.coroutines.runBlocking
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.time.Instant
import java.util.*
import java.util.concurrent.TimeUnit

public  class SampleSendTiket {
    companion object {


        @JvmStatic
        public fun main(args:Array<String>) {
            val q1=row("3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab1300","3424f5b3-e337-4d0d-a046-a1c491ab5510",12L,true,1.2, Instant.ofEpochMilli(295555080000),0)

            listOf(q1).stream().forEach {q-> publishKafka(q, "data.w3")  }
            println(" chunk data.w3 sended ")

        }
        @JvmStatic
        fun publishKafka(u: row, topic: String){
            val prod = getPproducer()
            prod?.send(ProducerRecord<String?, SpecificRecord?>(topic, u.getId(), u))
            prod?.flush()
            prod?.close()


        }

        @JvmStatic
        open fun getPproducer(): Producer<String?, SpecificRecord?>? {
            val properties = Properties()

            //properties["metadata.max.idle.ms"] = "1000000000"
            //properties["metadata.max.age.ms"] = 1000_000_000 //5min
            //properties["connections.max.idle.ms"] = 1000_000_000 //9 min


            properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "t5-ads-02.ru-central1.internal:9092"
            properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
            properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = KafkaAvroSerializer::class.java
            return KafkaProducer(properties)
        }
    }
}