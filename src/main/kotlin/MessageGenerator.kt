import datamart.query.VehicleRegData

import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.lang.Long
import java.util.*
import java.util.concurrent.Executors
import java.util.concurrent.Future
import kotlin.math.roundToInt

class MessageGenerator {




    private fun newProducer(bootstrap: String): Producer<String, String/*SpecificRecord*/> {
        val properties: java.util.Properties = java.util.Properties()
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrap)
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
//        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer::class.java)
        properties.put(ProducerConfig.LINGER_MS_CONFIG,10)
        properties.put(ProducerConfig.BATCH_SIZE_CONFIG,100)
        properties.put(ProducerConfig.ACKS_CONFIG,"1")
        return KafkaProducer(properties)
    }


    fun generate(bootstrap: String ="p2-ads-04.load.local:9092", threadCount:Int =1, count:Int =5000) {
        val tp=Executors.newFixedThreadPool(threadCount)
        //var producer: Producer<String, SpecificRecord> = newProducer(bootstrap)
        //var producer: Producer<String, String> = newProducer(bootstrap)
        var producerList = mutableListOf<Producer<String, String>>()
        for (i in 1..threadCount)
            producerList.add(newProducer(bootstrap))
        val tstart = java.lang.System.currentTimeMillis()
        var list =mutableListOf<Future<*>>()
        //val count = 5000
        var percent:Int = 0
        for (i in 1..count)
         { val f =tp.submit() {
            val vehicle: VehicleRegData = getVehicle(Long(i.toLong()))
            publishKafka(producerList.get(i%threadCount),vehicle, "test.topic")
            val newPrc=(100.0*i/count).roundToInt()

            if (newPrc!=percent) {
                percent = newPrc
                println(" $percent %")
            }

        }
             list.add(f)
        }
        for(f in list)
        {f.get()}
        val durationMs = java.lang.System.currentTimeMillis() - tstart
        val rps = count * 1000.0/durationMs
                println("messages=$count, rps=$rps ")


    }

    private fun publishKafka(prod: Producer<String, String/*SpecificRecord*/>, u: VehicleRegData, topic: String) {
        //prod.send(ProducerRecord(topic, Long.toString(u.getReestrid()), u))
        prod.send(ProducerRecord(topic, Long.toString(u.getReestrid()), "1"))
        prod.flush()
    }
}

fun main(arg: Array<String>) {
    if (arg.size>2) MessageGenerator().generate(arg[0],arg[1].toInt(),arg[2].toInt())
    else if (arg.size>1) MessageGenerator().generate(arg[0],arg[1].toInt())
    else if (arg.size>0) MessageGenerator().generate(arg[0])
    else MessageGenerator().generate()
    System.exit(0)
}



    
