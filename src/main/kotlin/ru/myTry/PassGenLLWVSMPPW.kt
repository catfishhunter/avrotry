package ru.myTry

import demo.rowPassenger
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import org.springframework.boot.jdbc.DataSourceBuilder
import org.springframework.jdbc.core.JdbcTemplate
import org.springframework.jdbc.core.PreparedStatementSetter
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import java.lang.StringBuilder
import java.sql.Date
import java.time.Duration
import java.time.Instant
import java.time.LocalDate
import java.util.*
import java.util.concurrent.atomic.AtomicLong
import javax.sql.DataSource
import kotlin.collections.HashMap
import kotlin.concurrent.thread
import java.sql.Connection
import java.sql.DriverManager


class PassGenLLWVSMPPW {
    companion object {
        var first: StringBuilder = StringBuilder();

        @JvmStatic
        public fun main(args: Array<String>) {
            generate(args)
        }
    }
}


fun generate(arg: Array<String>) {
    //val codesFiles = FileWriter("codes.csv")
    val threadCount: Int = if (arg.size > 0) arg[0].toInt() else 1
    val messagesByTreadcount = if (arg.size > 1) arg[1].toLong() else 1L
    val loadMode = if (arg.size > 2) LoadMode.valueOf(arg[2]) else LoadMode.KAFKA
    val size = if (arg.size > 3) PassengerSize.valueOf(arg[3]) else PassengerSize.S100B
    val batchSize = if (arg.size > 4) arg[4].toInt() else 1
    val warmSize = if (arg.size > 5) arg[5].toInt() else 0

    lateinit var prod: Producer<String?, List<SpecificRecord>?>
    if (loadMode == LoadMode.KAFKA) prod = getPproducerStandalone()
    //val conn = getJDBCConnection()
    //val pc = conn.prepareCall ("insert into dbllw.passenger values(?,?,?,?,?,?,?,?)");*/

    val jdbcTemplate = JdbcTemplate(getDataSourceProstore());
    val jdbcTemplateNamed = NamedParameterJdbcTemplate(getDataSourceProstore());
    val params = HashMap<String, Any>();

    println("!!! using mode threadCount=$threadCount messageByTreadcount=$messagesByTreadcount  loadMode=$loadMode size=$size  ")

    fillFirst(size)

    if (loadMode == LoadMode.LLW || loadMode == LoadMode.LLWBATCH) jdbcTemplateNamed.update(
        "delete from dbllw.passenger", HashMap<String, Any>()
    )
    if ( loadMode == LoadMode.LLWBATCHPARAM) jdbcTemplate.update(
        "delete from dbllw.passenger"
    )

    var badPass = passenger(
        0L, UUID.randomUUID().toString()
    )
    if (loadMode == LoadMode.LLW) for (u in -1*warmSize..-1) {
        badPass.setCode(u.toLong())
        senByJDBCTemplate(badPass, jdbcTemplateNamed)
    }
    if (loadMode == LoadMode.LLWBATCH) {
        var batch = mutableListOf<demo.rowPassenger>()
        for (u in -1*warmSize..-1) {
            badPass = passenger(u.toLong(), UUID.randomUUID().toString())
            batch.add(badPass)
        }
        if (warmSize>0) senByJDBCTemplatestring(batch, jdbcTemplateNamed)
    }
    if (loadMode == LoadMode.LLWBATCHPARAM) {
        var batch = mutableListOf<demo.rowPassenger>()
        for (u in -1*warmSize..-1) {
            badPass = passenger(u.toLong(), UUID.randomUUID().toString())
            batch.add(badPass)
        }
        if (warmSize>0) senByJDBCTemplate(batch, jdbcTemplate)
    }

    try {
        val threads = mutableListOf<Thread>()
        val duration = AtomicLong(0)
        for (t in 1..threadCount) {
            var thread = thread {
                val base = (t - 1) * messagesByTreadcount
                //val random = Random(Long.MAX_VALUE - t)
                var batch = mutableListOf<demo.rowPassenger>()

                for (i in 1..messagesByTreadcount) {
                    val code = base + i// random.nextLong(0, Long.MAX_VALUE)
                    val id = UUID.randomUUID().toString()
                    val pass = passenger(code, id)
                    when (loadMode) {
                        LoadMode.LLW -> {
                            val (updated, insDuration) = senByJDBCTemplate(pass, jdbcTemplateNamed)
                            duration.addAndGet(insDuration)
                            println("$insDuration")
                        }

                        LoadMode.LLWBATCH -> {
                            batch.add(pass)
                            if (batch.size >= batchSize) {
                                val (updated, insDuration) = senByJDBCTemplatestring(batch, jdbcTemplateNamed)
                                duration.addAndGet(insDuration)
                                println("$insDuration")
                                batch = mutableListOf<demo.rowPassenger>()
                            }
                        }
                        LoadMode.LLWBATCHPARAM -> {
                            batch.add(pass)
                            if (batch.size >= batchSize) {
                                val (updated, insDuration) = senByJDBCTemplate(batch, jdbcTemplate)
                                duration.addAndGet(insDuration)
                                println("$insDuration")
                                batch = mutableListOf<demo.rowPassenger>()
                            }
                        }

                        LoadMode.KAFKA -> {
                            batch.add(pass)
                            if (batch.size >= batchSize) {
                                publishStandalone(batch, prod)
                                batch = mutableListOf<demo.rowPassenger>()
                            }
                        }
                    }

                    if (t == 1) println("$i from $messagesByTreadcount")
                    //codesFiles.append("$code \n")
                }
                when (loadMode) {
                    LoadMode.LLWBATCH -> if (batch.size > 0) {
                        val (updated, insDuration) = senByJDBCTemplatestring(batch, jdbcTemplateNamed)
                        duration.addAndGet(insDuration)
                        println("$insDuration")
                    }
                    LoadMode.LLWBATCHPARAM -> if (batch.size > 0) {
                        val (updated, insDuration) = senByJDBCTemplate(batch, jdbcTemplate)
                        duration.addAndGet(insDuration)
                        println("$insDuration")
                    }

                    LoadMode.KAFKA -> if (batch.size > 0) publishStandalone(batch, prod)
                }
            }

            threads.add(thread)
        }

        for (thread in threads) thread.join()
        println(" duration $duration ms")
        println("!!! using mode threadCount=$threadCount messageByTreadcount=$messagesByTreadcount  loadMode=$loadMode size=$size  ")

    } finally {

        if (loadMode == LoadMode.KAFKA) {
            prod.flush()
            prod.close()
        }

    }
}

private fun fillFirst(size: PassengerSize) {
    when (size) {
        PassengerSize.S100B -> PassGenLLWVSMPPW.first.append("Дональд")
        PassengerSize.S10K -> {
            for (i in 1..1000) PassGenLLWVSMPPW.first.append("10k10k10k_")
        }
        PassengerSize.S500K -> {
            for (i in 1..50000) PassGenLLWVSMPPW.first.append("10k10k10k_")
        }
        PassengerSize.S900K -> {
            for (i in 1..90000) PassGenLLWVSMPPW.first.append("10k10k10k_")
        }

        PassengerSize.S1M -> {
            for (i in 1..100000) PassGenLLWVSMPPW.first.append("1m1m1m1m1m")
        }

        PassengerSize.S5M -> {
            for (i in 1..500000) PassGenLLWVSMPPW.first.append("5m5m5m5m5m")
        }

        PassengerSize.S10M -> {
            for (i in 1..1000000) PassGenLLWVSMPPW.first.append("010m010m__")
        }

        PassengerSize.S20M -> {
            for (i in 1..2000000) PassGenLLWVSMPPW.first.append("020m020m__")
        }

        PassengerSize.S30M -> {
            for (i in 1..3000000) PassGenLLWVSMPPW.first.append("030m030m__")
        }

        PassengerSize.S40M -> {
            for (i in 1..4000000) PassGenLLWVSMPPW.first.append("040m040m__")
        }

        PassengerSize.S50M -> {
            for (i in 1..5000000) PassGenLLWVSMPPW.first.append("050m050m__")
        }

        PassengerSize.S100M -> {
            for (i in 1..10000000) PassGenLLWVSMPPW.first.append("100m100m__")
        }

        PassengerSize.S150M -> {
            for (i in 1..15000000) PassGenLLWVSMPPW.first.append("150m150m__")
        }

    }
}


private fun passenger(code: Long, id: String): rowPassenger {

    val pass = rowPassenger(
        code,//8
        id,//36
        PassGenLLWVSMPPW.first.toString(),//7
        "Байденлин",//6
        "Абдулсалманович",//15
        LocalDate.now(),//8 (4)
        "/url/passport",//13
       // 125 //4
    1
    )
    return pass
}

fun senByJDBCTemplate(
    pass: MutableList<rowPassenger>, jdbcTemplate: JdbcTemplate
): Pair<Int, Long> {

    var sql = StringBuilder("insert into dbllw.passenger values ")
    val params= mutableListOf<Any>()
    //val setter=PreparedStatementSetter()
    for (p in pass) {
        params.addAll(mutableListOf(p.getCode(),p.getId(),p.getFirstname(),p.getMiddlename(),p.getLastname(),LocalDate.of(p.getBirthday().year,p.getBirthday().monthValue,p.getBirthday().dayOfMonth),p.getPassport()))
        sql.append("(?,?,?,?,?,?,?),")
    }

    val tstart = Instant.now()
    println("executing batch sql length: ${sql.length}")
    val updated = jdbcTemplate.update(sql.trim(',').toString(), *params.toTypedArray());
    println("executed batch sql ")

    val duration = Duration.between(tstart, Instant.now()).toMillis()
    return Pair(updated, duration)
}

fun senByJDBCTemplatestring(
    pass: MutableList<rowPassenger>, jdbcTemplate: NamedParameterJdbcTemplate
): Pair<Int, Long> {
    val params = HashMap<String, Any>()
    var sql = StringBuilder("insert into dbllw.passenger values ")
    for (p in pass) sql.append("(${p.getCode()},'${p.getId()}','${p.getFirstname()}','${p.getMiddlename()}','${p.getLastname()}','${p.getBirthday().year}-${p.getBirthday().monthValue}-${p.getBirthday().dayOfMonth}','${p.getPassport()}'),")
    val tstart = Instant.now()
    println("executing batch sql length: ${sql.length}")
    val updated = jdbcTemplate.update(sql.trim(',').toString(), params);
    println("executed batch sql ")

    val duration = Duration.between(tstart, Instant.now()).toMillis()
    return Pair(updated, duration)
}

private fun senByJDBCTemplate(
    pass: rowPassenger, jdbcTemplate: NamedParameterJdbcTemplate
): Pair<Int, Long> {
    val params = HashMap<String, Any>()
    params.put("code", pass.getCode());
    params.put("id", pass.getId());
    params.put("f", pass.getFirstname());
    params.put("m", pass.getMiddlename());
    params.put("l", pass.getLastname());
    params.put("b", Date.valueOf(pass.getBirthday()));
    params.put("p", pass.getPassport());
    val tstart = Instant.now()
    val updated = jdbcTemplate.update("insert into dbllw.passenger values(:code,:id,:f,:m,:l,:b,:p)", params);
    val duration = Duration.between(tstart, Instant.now()).toMillis()
    return Pair(updated, duration)
}

private const val PROSTORE = "jdbc:prostore://172.17.38.69:9195"

private fun getJDBCConnection(): Connection? {
    Class.forName("ru.datamart.prostore.jdbc.Driver")//.kotlin
    val conn = DriverManager.getConnection(PROSTORE)
    return conn
}
fun getDataSourceProstore(): DataSource? {
    val dataSourceBuilder = DataSourceBuilder.create()
    dataSourceBuilder.driverClassName("ru.datamart.prostore.jdbc.Driver")
    dataSourceBuilder.url(PROSTORE)
    return dataSourceBuilder.build()
}

fun publishStandalone(batch: MutableList<demo.rowPassenger>, prod: Producer<String?, List<SpecificRecord>?>) {
    val topic = "data.w1"
    try {
        //prod.send(ProducerRecord<String?, List<SpecificRecord>?>(topic, UUID.randomUUID().toString(), batch))
        prod.send(ProducerRecord<String?, List<SpecificRecord>?>(topic, UUID.randomUUID().toString(), batch)
        ) { recordMetadata, e ->
            if (e != null) {
                e.printStackTrace()
            }
        }
        prod.flush()
    } catch (e: Exception) {
        e.printStackTrace()
        throw e
    }

}



fun getPproducerStandalone(): Producer<String?, List<SpecificRecord>?> {
    val properties = Properties()

    //properties["metadata.max.idle.ms"] = "1000000000"
    //properties["metadata.max.age.ms"] = 1000_000_000 //5min
    //properties["connections.max.idle.ms"] = 1000_000_000 //9 min
    //properties[ProducerConfig.ACKS_CONFIG] = "all"
    properties[ProducerConfig.MAX_REQUEST_SIZE_CONFIG]=136314880
    properties[ProducerConfig.BUFFER_MEMORY_CONFIG]=136314880

    //properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "p2-ads-04.load.local:9092"
    properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "10.81.7.90:12641"//""t5-ads-01.ru-central1.internal:9092"

    properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
    properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = KafkaAvroListSerializer::class.java
    return KafkaProducer(properties)
}

enum class LoadMode { KAFKA, LLW, LLWBATCH, LLWBATCHPARAM }
enum class PassengerSize { S100B, S10K, S500K, S900K, S1M, S5M, S10M, S20M, S30M, S40M, S50M, S100M, S150M }



