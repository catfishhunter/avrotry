import datamart.query.VehicleRegData
import example.avro.Cat
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.io.PrintWriter
import java.lang.Long
import java.time.LocalDate
import java.util.*

class TopicsGenerator {


    val producer: Producer<String, SpecificRecord> =newProducer()

    private fun newProducer():Producer<String, SpecificRecord> {
        val properties: java.util.Properties = java.util.Properties()
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "p2-ads-04.load.local:9092")
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaAvroSerializer::class.java)
        return KafkaProducer(properties)
    }
    private fun getPproducer(): Producer<String, SpecificRecord> {
        return producer
    }
    fun generate() {
        for (i in 1..50) {
            val vehicle: VehicleRegData = getVehicle(Long(i.toLong()))
            publishKafka(vehicle,"test.mppw.$i")
        }
    }
    private fun publishKafka(u: VehicleRegData,topic: String) {
        val prod: Producer<String, SpecificRecord> = getPproducer()
        prod.send(ProducerRecord(topic, Long.toString(u.getReestrid()), u))
        prod.flush()
    }
}

fun main( arg:Array<String>) {
    TopicsGenerator().generate()
}

fun getVehicle( i:Long ):VehicleRegData{
    val d: LocalDate=LocalDate.of(2005,1,1)
    //val i = 0L
    return VehicleRegData(i.toLong(),
        "dbf09287-d2bd-4979-b098-c9b59c36e8d7",
        "AK143 750",
        "0",
        "Транспортные средства должностных лиц высших органов государственной власти Российской Федерации",
        "1GT221HL5BMVDB7K9",
        "1B7KC26Z9V3YTKA70",
        "VDB7K9",
        "2009",
        "70",
        "Водительское удостоверение",
        "9483428572",
        d,
        "МО ГИБДД ТНРЭР №2 ГУ МВД России",
        "Висеть хлеб поздравлять.",
        20,
        "Паспорт транспортного средства",
        "82 ШХ 964902",
        d,
        "Приспособление интерактивных интерфейсов",
        "",
        "Белый",
        0,
        10103,
        101009,
        "AMC-03",
        "0A",
        "AMC-03 0A",
        "1GT221HL5BMVDB7K9",
        "3306552.0",
        "vehiclegasequip",
        101,
        "Корпорация AMC",
        100,
        100,
        100,
        100,
        100,
        100,
        "vehicletypename",
        100,
        "vehiclecategory",
        100,
        "vehicletypeunit",
        100,
        "vehiclespecfuncname",
        100,
        "vehicleenginemodel",
        "vehicleenginenum",
        1.1f,
        1.2f,
        100,
        "vehicleenginetype",
        100,
        100,
        "ownerpersondocnum",
        d,
        "ownerpersondocissuer",
        "ownerpersonlastname",
        "ownerpersonfirstname",
        "ownerpersonmiddlename",
        d,
        100,
        100,
        "ownerbirthplace",
        "ownerpersoninn",
        "ownerpersonsnils",
        100,
        "ownerpersonogrnip",
        "owneraddressguid",
        100,
        "owneraddressregionname",
        "owneraddressdistrict",
        "owneraddressmundistrict",
        "owneraddresssettlement",
        "owneraddressstreet",
        "owneraddressbuilding",
        100,
        "owneraddressstructurename",
        "owneraddressstructure",
        100,
        "owneraddressestatename",
        "owneraddressestate",
        100,
        "owneraddressflatname",
        "owneraddressflat",
        100,
        "ownerorgname",
        d,
        100,
        "ownerinn",
        "ownerkpp",
        "ownerogrn",
        "ownermainorgname",
        "ownermainorginn",
        "ownermainorgkpp",
        "ownermainorgogrn",
        "ownermainaddressguid",
        100,
        "ownermainaddressregionname",
        "ownermainaddressdistrict",
        "ownermainaddressmundistrict",
        "ownermainaddresssettlement",
        "ownermainaddressstreet",
        "ownermainaddressbuilding",
        100,
        "ownermainaddressstructurename",
        "ownermainaddressstructure",
        100,
        "ownermainaddressestatename",
        "ownermainaddressestate",
        100,
        "ownermainaddressflatname",
        "ownermainaddressflat",
        100,
        100,
        100,
        "holderpersondocnum",
        d,
        "holderpersondocissuer",
        "holderpersonlastname",
        "holderpersonfirstname",
        "holderpersonmiddlename",
        d,
        100,
        100,
        "holderpersonbirthplace",
        "holderpersoninn",
        "holderpersonsnils",
        "holderpersonogrnip",
        "holderaddressguid",
        100,
        "holderaddressregionname",
        "holderaddressdistrict",
        "holderaddressmundistrict",
        "holderaddresssettlement",
        "holderaddressstreet",
        "holderaddressbuilding",
        100,
        "holderaddressstructurename",
        "holderaddressstructure",
        100,
        "holderaddressestatename",
        "holderaddressestate",
        100,
        "holderaddressflatname",
        "holderaddressflat",
        100,
        "holderorgname",
        d,
        100,
        "holderorginn",
        "holderorgkpp",
        "holderorgogrn",
        100,
        "holdermainorgname",
        "holdermainorginn",
        "holdermainorgkpp",
        "holdermainorgogrn",
        "holdermainaddressguid",
        100,
        "holdermainaddressregionname",
        "holdermainaddressdistrict",
        "holdermainaddressmundistrict",
        "holdermainaddresssettlement",
        "holdermainaddressstreet",
        "holdermainaddressbuilding",
        100,
        "holdermainaddressstructurename",
        "holdermainaddressstructure",
        100,
        "holdermainaddressestatename",
        "holdermainaddressestate",
        100,
        "holdermainaddressflatname",
        "holdermainaddressflat",
        100,
        d,
        "approvalnum",
        d,
        "approvaltype",
        100,
        "utilizationfeename",
        "customsdoc",
        d,
        "customsdocissue",
        "customsdocrestriction",
        100,
        "customscountryremovalname",
        100,
        "operationname",
        100,
        100,
        "operationdepartmentname",
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        100,
        //"norm_vehiclevin",
        //"norm_vehiclevin2",
        //"norm_vehiclebodynum",
       // "norm_vehiclechassisnum",
        //"norm_vehicleregno",
    0
    )
    /*return VehicleRegData.newBuilder()
        .setReestrid(i.toLong())
        .setVehicleguid("dbf09287-d2bd-4979-b098-c9b59c36e8d7")
        .setVehicleregno("AK143 750")
        .setVehicleregnotypeid("0")
        .setVehicleregnoname("0")
        .setVehiclechassisnum("")
        .setVehiclereleaseyear("")
        .setVehiclevin("1GT221HL5BMVDB7K9")
        .setVehiclevin2("1GT221HL5BMVDB7K9")
        .setOperationregdoctypeid("")
        .setSysOp(0)
        .build()*/
}

    
