/*
Таблицы наполняются генерируемыми данными, соответствующими схеме РМИС, количество записей patient - 5 млн., где:
lastname случайная заглавная буква русского алфавита
middlename случайная заглавная буква русского алфавита
birthdate случайное число 2000 года
oms_number случайное десятизначное число
*/

package RMISGenerator

import java.io.PrintWriter
import java.time.LocalDate
import java.time.format.DateTimeFormatter


class CSVGeneratorPatient {
}

fun main( arg:Array<String>)
{
val writer = PrintWriter("patient.csv")
val writerjem = PrintWriter("patientjem.csv")
try {

    val s1 ="id;firstname;lastname;middlename;snils;oms_region;oms_number;birthdate;gender;norm_firstname;norm_lastname;norm_middlename;create_ts;update_ts"
    val s1jem ="id;firstname;lastname;oms_number;birthdate"
    writer.append("$s1\n")
    writerjem.append("$s1jem\n")

//1..5000000
    for (i in 1..100) {
        val firstName=rndLetter()
        val lastName=rndLetter()
        val omsN= rndOms()
        val birth = rndDate()
        val s = """$i;$firstName;$lastName;Middle;6312220000;oms777;$omsN;$birth;MALE;Ivan;Ivanovich;Ivanov;2021-11-17 10:47:28;2023-01-11 16:13:44""".replace("\n", "").replace("    ", "")
        val sjem = """$i;$firstName;$lastName;$omsN;$birth""".replace("\n", "").replace("    ", "")
       // println(s)
        writer.append("$s\n")
        writerjem.append("$sjem\n")
    }
}finally {
    writer.close()
    writerjem.close()
}

}
fun rndLetter()= ('А'..'Я').random()
fun rndDate():String {
    var d1 = LocalDate.of(2000, 1, 1)
    return "${d1.plusDays((0L..364).random()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))} 00:00:00"
}
fun rndOms()=(1000000000..9999999999).random()


    
