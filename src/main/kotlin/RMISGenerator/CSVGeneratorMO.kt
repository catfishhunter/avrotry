/*
Таблицы наполняются генерируемыми данными, соответствующими схеме РМИС, количество записей mo - 100 тыс., где:
name одно из (на месте троеточия случайное число 1-100к)
"ГКБ ... Врачебная амбулатория"
"ГКБ ... Приемно-диагностическое отделение"
"ГКБ ... Кардиология"
*/

package RMISGenerator

import jdk.nashorn.internal.ir.annotations.Immutable
import java.io.PrintWriter

class CSVGeneratorMO {
}

fun main( arg:Array<String>)
{
    val writer = PrintWriter("mo.csv")
    val writerjem = PrintWriter("mojem.csv")
try {

    val s1 ="parent_id;update_ts;address;address_fias_guid;enabled;name;region_okato;create_ts;id;rmis_id;phone"
    val s1jem ="name;id"
    writer.append("$s1\n")
    writerjem.append("$s1jem\n")

//1..100000
    for (i in 1..100000) {
        val name= rndName()
        val s = """null;2022-09-02 00:00:00;ленина-5;fiasaddr;yes;$name;okato;2022-09-02 00:00:00;$i;rmisid;322223322""".replace("\n", "").replace("    ", "")
        val sjem = """$name;$i""".replace("\n", "").replace("    ", "")
       // println(s)
        writer.append("$s\n")
        writerjem.append("$sjem\n")
    }
}finally {
    writer.close()
    writerjem.close()
}

}

fun rndName():String  =
    listOf<String>("ГКБ %d Врачебная амбулатория",
        "ГКБ %d Приемно-диагностическое отделение",
        "ГКБ %d Кардиология").random().format((1..100000).random())


    
