import org.slf4j.LoggerFactory
import java.math.BigDecimal


class MyTry {
}

public fun main(vararg a: String){

    var pers = BigDecimal(1234567890.25)

    var s = pers.scale()
    var i = pers.precision()
    println("${pers} i=$i  s=$s")

    val log=LoggerFactory.getLogger("MyTryKt");
    log.info("yohoho")
    log.info("and buttle of rom")

    val p= listOf(2 to "eee", 5 to "uuu")

    println(p)
    with(p) { println(get(0))}

    val sb= StringBuilder().apply{
        for (letter in 'A'..'Z'){
            append(letter)
        }
    }.toString()
    println(sb)

    val sb1= with(StringBuilder()){
        for (letter in 'A'..'Z'){
            append(letter)
        }
            toString().substring(2..4)+"aaa"
    }
    println(sb1)

    val game: String = StringBuilder().also {
        it.append("tic")
        it.append("tac")
        it.append("toe")
    }
        .toString()

    println(game)

    var tu: String?=null
    val fallbackResponse = tu?.let {
        "new string $it"
    }?:"rrr"
    println(fallbackResponse)
}