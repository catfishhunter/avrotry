import datamart.query.QueryRequest
import example.avro.Cat
//import jdk.nashorn.internal.objects.NativeArray
import kotlinx.coroutines.runBlocking
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.util.*
import java.util.concurrent.TimeUnit

public  class SampleSend {
    companion object {

        @JvmStatic
        public fun main(args:Array<String>) {
            val cat1=Cat(2,5,"Murzik3",0)
            val cat2= Cat(3,15,"Tom3",0)
            val cat3= Cat(4,25,"Barsik3",0)
            println(" one publish to 3cats2")
            listOf(cat1,cat2,cat3).stream().forEach {cat-> publishKafka(cat, "3cats")  }
            println(" please remove the topic 3cats2 manually now ")

            for (i in 1..15)
                runBlocking{
                    TimeUnit.MINUTES.sleep(1)
                    println(" $i tik")
//                  listOf(cat1).stream().forEach {cat-> publishKafka(cat, "3hlam")  }
                }

        }
        @JvmStatic
        fun publishKafka(u: Cat, topic: String){
            val prod = getPproducer()
            prod?.send(ProducerRecord<String?, SpecificRecord?>(topic, java.lang.Long.toString(u.getId()), u))
            prod?.flush()
            prod?.close()


        }

        @JvmStatic
        open fun getPproducer(): Producer<String?, SpecificRecord?>? {
            val properties = Properties()

            //properties["metadata.max.idle.ms"] = "1000000000"
            //properties["metadata.max.age.ms"] = 1000_000_000 //5min
            //properties["connections.max.idle.ms"] = 1000_000_000 //9 min


            //properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "t5-one-04.ru-central1.internal:9092"
            properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "10.81.7.90:12541"
            //properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "p2-ads-04.load.local:9092"
            properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
            properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = KafkaAvroSerializer::class.java
            return KafkaProducer(properties)
        }
    }
}