import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.io.FileWriter
import java.time.Instant
import java.time.LocalDate
import java.util.*
import kotlin.concurrent.thread
import kotlin.random.Random
import kotlin.random.nextULong

fun main(arg: Array<String>) {
    //val codesFiles = FileWriter("codes.csv")
    val prod = getPproducer()
    try {

        val threadCount: Int = 1
        val threads = mutableListOf<Thread>()
        for (t in 1..threadCount) {
            var thread = thread {
                val count = 40000L
                val base =10000000+(t-1)*count
                val batchSize = 10
                //val random = Random(Long.MAX_VALUE - t)
                var batch = mutableListOf<demo.rowPassenger>()
                for (i in 1..count) {
                    val code = base+i// random.nextLong(0, Long.MAX_VALUE)
                    val id = UUID.randomUUID().toString()
                    val pass = demo.rowPassenger(
                        code,
                        id,
                        "Дональд",
                        "Байден",
                        "Абдулсалманович",
                        LocalDate.now(),
                        "/url/passport",
                       // 125,
                        0
                    )
                    batch.add(pass)
                    if (batch.size >= batchSize) {
                        publish(batch,prod)
                        batch = mutableListOf<demo.rowPassenger>()
                    }
                    if (t == 1) println("$i from $count")
                    //codesFiles.append("$code \n")
                }
                if (batch.size > 0)
                    publish(batch,prod)
            }

            threads.add(thread)
        }

        for (thread in threads)
            thread.join()

    } finally {
       // codesFiles.close()
        prod.flush()
        prod.close()

    }
}

fun publish(batch: MutableList<demo.rowPassenger>,prod: Producer<String?, List<SpecificRecord>?> ) {
    val topic = "data.w5"
    //for (p in batch)
    prod.send(ProducerRecord<String?, List<SpecificRecord>?>(topic, UUID.randomUUID().toString(), batch))

}

fun getPproducer(): Producer<String?, List<SpecificRecord>?> {
    val properties = Properties()

    //properties["metadata.max.idle.ms"] = "1000000000"
    //properties["metadata.max.age.ms"] = 1000_000_000 //5min
    //properties["connections.max.idle.ms"] = 1000_000_000 //9 min


    properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "demo-dtm-kz01.ru-central1.internal:9092"
    properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
    properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = ru.myTry.KafkaAvroListSerializer::class.java
    return KafkaProducer(properties)
}

class PassengerGenerator {

}

