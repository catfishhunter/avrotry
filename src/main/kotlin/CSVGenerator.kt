import java.io.PrintWriter
import kotlin.random.Random

class CSVGenerator {
}

fun main( arg:Array<String>)
{
val writer = PrintWriter("data.csv")
try {
    var s1 =
        "reestrid;vehicleguid;vehicleregno;vehicleregnotypeid;vehicleregnoname;vehiclevin;vehiclevin2;vehiclechassisnum;vehiclereleaseyear;operationregdoctypeid;operationregdoctypename;operationregdoc;operationregdocissuedate;operationregdocissuer;operationregdoccomments;vehicleptstypeid;vehicleptstypename;vehicleptsnum;vehicleptsissuedate;vehicleptsissuer;vehicleptscomments;vehiclebodycolor;vehiclebodycolorgroupid;vehiclebrandid;vehiclemodelid;vehiclebrand;vehiclemodel;vehiclebrandmodel;vehiclebodynum;vehiclecost;vehiclegasequip;vehicleproducerid;vehicleproducername;vehiclegrossmass;vehiclemass;vehiclesteeringwheeltypeid;vehiclekpptype;vehicletransmissiontype;vehicletypecode;vehicletypename;vehiclecategoryid;vehiclecategory;vehicletypeunitcode;vehicletypeunit;vehicleecoclass;vehiclespecfuncname;vehicleenclosedvolume;vehicleenginemodel;vehicleenginenum;vehicleenginepower;vehicleenginepowerkw;vehicleenginetypeid;vehicleenginetype;ownercode;ownerpersondoccode;ownerpersondocnum;ownerpersondocdate;ownerpersondocissuer;ownerpersonlastname;ownerpersonfirstname;ownerpersonmiddlename;ownerpersonbirthdate;ownerpersonbirthregionid;ownerpersonsex;ownerbirthplace;ownerpersoninn;ownerpersonsnils;ownerpersonip;ownerpersonogrnip;owneraddressguid;owneraddressregionid;owneraddressregionname;owneraddressdistrict;owneraddressmundistrict;owneraddresssettlement;owneraddressstreet;owneraddressbuilding;owneraddressstructureid;owneraddressstructurename;owneraddressstructure;owneraddressestateid;owneraddressestatename;owneraddressestate;owneraddressflatid;owneraddressflatname;owneraddressflat;owneraddressindex;ownerorgname;ownerorgdatecreate;ownerorgbranch;ownerinn;ownerkpp;ownerogrn;ownermainorgname;ownermainorginn;ownermainorgkpp;ownermainorgogrn;ownermainaddressguid;ownermainaddressregionid;ownermainaddressregionname;ownermainaddressdistrict;ownermainaddressmundistrict;ownermainaddresssettlement;ownermainaddressstreet;ownermainaddressbuilding;ownermainaddressstructureid;ownermainaddressstructurename;ownermainaddressstructure;ownermainaddressestateid;ownermainaddressestatename;ownermainaddressestate;ownermainaddressflatid;ownermainaddressflatname;ownermainaddressflat;ownermainaddressindex;holdertypeid;holderpersondoccode;holderpersondocnum;holderpersondocdate;holderpersondocissuer;holderpersonlastname;holderpersonfirstname;holderpersonmiddlename;holderpersonbirthdate;holderpersonbirthregionid;holderpersonsex;holderpersonbirthplace;holderpersoninn;holderpersonsnils;holderpersonogrnip;holderaddressguid;holderaddressregionid;holderaddressregionname;holderaddressdistrict;holderaddressmundistrict;holderaddresssettlement;holderaddressstreet;holderaddressbuilding;holderaddressstructureid;holderaddressstructurename;holderaddressstructure;holderaddressestateid;holderaddressestatename;holderaddressestate;holderaddressflatid;holderaddressflatname;holderaddressflat;holderaddressindex;holderorgname;holderorgdatecreate;holderorgbranch;holderorginn;holderorgkpp;holderorgogrn;holdermainid;holdermainorgname;holdermainorginn;holdermainorgkpp;holdermainorgogrn;holdermainaddressguid;holdermainaddressregionid;holdermainaddressregionname;holdermainaddressdistrict;holdermainaddressmundistrict;holdermainaddresssettlement;holdermainaddressstreet;holdermainaddressbuilding;holdermainaddressstructureid;holdermainaddressstructurename;holdermainaddressstructure;holdermainaddressestateid;holdermainaddressestatename;holdermainaddressestate;holdermainaddressflatid;holdermainaddressflatname;holdermainaddressflat;holdermainaddressindex;holdrestrictiondate;approvalnum;approvaldate;approvaltype;utilizationfeeid;utilizationfeename;customsdoc;customsdocdate;customsdocissue;customsdocrestriction;customscountryremovalid;customscountryremovalname;operationcode;operationname;operationdate;operationdepartmentcode;operationdepartmentname;operationordernum;operationstatus;operationattorney;operationlising;operationkid;reestrstatus;reestrbegindate;reestrenddate;reestriscurrent;reestrisexternal;reestrprevrecord;norm_vehiclevin;norm_vehiclevin2;norm_vehiclebodynum;norm_vehiclechassisnum;norm_vehicleregno"
    println(s1)
    writer.append("$s1\n")


    for (i in 1..100) {

        val r=(1..1000).random()

        var s = """$i;
    dbf09287-d2bd-4979-b098-c9b59c36e8d7;
    AK143 750;
    $r;
    Транспортные средства должностных лиц высших органов государственной власти Российской Федерации;
    1GT221HL5BMVDB7K9;
    1B7KC26Z9V3YTKA70;
    VDB7K9;
    2009;
    70;
    Водительское удостоверение;
    9483428572;
    05.03.2015;   
    МО ГИБДД ТНРЭР №2 ГУ МВД России;
    Висеть хлеб поздравлять.;
    20;
    Паспорт транспортного средства;
    82 ШХ 964902;
    05.03.2015;
    Приспособление интерактивных интерфейсов;
    ;
    Белый;
    0;
    10103;
    101009;
    AMC-03;
    0A; 
    AMC-03 0A;
    1GT221HL5BMVDB7K9;
    3306552.0;
    vehiclegasequip;
    101;
    Корпорация AMC;
    100;
    100;
    100;
    100;
    100;
    100; 
    vehicletypename;
    100;
    vehiclecategory;
    100;
    vehicletypeunit;
    100;
    vehiclespecfuncname;
    100;
    vehicleenginemodel;
    vehicleenginenum;
    1.1;
    1.2;
    100;
    vehicleenginetype;
    100;
    100;
    ownerpersondocnum;
    05.03.2015;
    ownerpersondocissuer;
    ownerpersonlastname;
    ownerpersonfirstname;
    ownerpersonmiddlename;
    05.03.2015;
    100;
    100;
    ownerbirthplace;
    ownerpersoninn;
    ownerpersonsnils; 
    100;
    ownerpersonogrnip;
    owneraddressguid;
    100;
    owneraddressregionname;
    owneraddressdistrict;
    owneraddressmundistrict;
    owneraddresssettlement;
    owneraddressstreet;
    owneraddressbuilding;
    100;
    owneraddressstructurename;
    owneraddressstructure;
    100;
    owneraddressestatename;
    owneraddressestate;
    100;
    owneraddressflatname;
    owneraddressflat;
    100;
    ownerorgname;
    05.03.2015;
    100;
    ownerinn;
    ownerkpp;
    ownerogrn;
    ownermainorgname;
    ownermainorginn;
    ownermainorgkpp;
    ownermainorgogrn;
    ownermainaddressguid;
    100;
    ownermainaddressregionname;
    ownermainaddressdistrict;
    ownermainaddressmundistrict;
    ownermainaddresssettlement;
    ownermainaddressstreet;
    ownermainaddressbuilding;
    100;
    ownermainaddressstructurename;
    ownermainaddressstructure;
    100;
    ownermainaddressestatename;
    ownermainaddressestate;
    100;
    ownermainaddressflatname;
    ownermainaddressflat;
    100;
    100;
    100;
    holderpersondocnum;
    05.03.2015;
    holderpersondocissuer;
    holderpersonlastname;
    holderpersonfirstname;
    holderpersonmiddlename;
    05.03.2015;
    100;
    100;
    holderpersonbirthplace;
    holderpersoninn;
    holderpersonsnils;
    holderpersonogrnip;
    holderaddressguid;
    100;
    holderaddressregionname;
    holderaddressdistrict;
    holderaddressmundistrict;
    holderaddresssettlement;
    holderaddressstreet;
    holderaddressbuilding;
    100;
    holderaddressstructurename;
    holderaddressstructure;
    100;
    holderaddressestatename;
    holderaddressestate;
    100;
    holderaddressflatname;
    holderaddressflat;
    100;
    holderorgname;
    05.03.2015;
    100;
    holderorginn;
    holderorgkpp;
    holderorgogrn;
    100;
    holdermainorgname;
    holdermainorginn;
    holdermainorgkpp;
    holdermainorgogrn;
    holdermainaddressguid;
    100;
    holdermainaddressregionname;
    holdermainaddressdistrict;
    holdermainaddressmundistrict;
    holdermainaddresssettlement;
    holdermainaddressstreet;
    holdermainaddressbuilding;
    100;
    holdermainaddressstructurename;
    holdermainaddressstructure;
    100;
    holdermainaddressestatename;
    holdermainaddressestate;
    100;
    holdermainaddressflatname;
    holdermainaddressflat;
    100;
    05.03.2015;
    approvalnum;
    05.03.2015;
    approvaltype;
    100;
    utilizationfeename;
    customsdoc;
    05.03.2015;
    customsdocissue;
    customsdocrestriction;
    100;
    customscountryremovalname;
    100;
    operationname;
    20.09.2012;
    100;
    operationdepartmentname;
    100;
    100;
    100;
    100;
    100;
    100;
    20.09.2012;
    20.09.2012;
    100;
    100;
    100;
    norm_vehiclevin;
    norm_vehiclevin2;
    norm_vehiclebodynum;
    norm_vehiclechassisnum;
    norm_vehicleregno    
    """.replace("\n", "").replace("    ", "")
       // println(s)
        writer.append("$s\n")
    }
}finally {
    writer.close()
}

}
   /*

    */

    
