//import datamart.query.QueryRequest
import datamart.query.QueryResultChunk
import datamart.query.QueryResultRow
import datamart.query.VehicleRegData
import example.avro.Cat
import org.apache.avro.file.DataFileReader
import org.apache.avro.io.DatumReader
import org.apache.avro.specific.SpecificDatumReader
import java.io.File

class AvroRenderer {
}

fun main(args: Array<String>){
    renderAvro()
}
fun renderAvro(){

    //val myDatumReader: DatumReader<QueryResultChunk> = SpecificDatumReader(QueryResultChunk::class.java)
    val myDatumReader: DatumReader<QueryResultRow> = SpecificDatumReader(QueryResultRow::class.java)
    val dataFileReader = DataFileReader(File("responseValue.avro"), myDatumReader)
    var veh: QueryResultRow? = null
    while (dataFileReader.hasNext()) {
        veh = dataFileReader.next(veh)
        println(veh)
    }
}