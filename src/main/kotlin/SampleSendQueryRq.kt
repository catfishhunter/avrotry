import datamart.query.QueryRequest
import example.avro.Cat
//import jdk.nashorn.internal.objects.NativeArray
import kotlinx.coroutines.runBlocking
import org.apache.avro.specific.SpecificRecord
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringSerializer
import java.util.*
import java.util.concurrent.TimeUnit

public  class SampleSendQueryRq {
    companion object {


        @JvmStatic
        public fun main(args:Array<String>) {
            //val q1=QueryRequest("3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","demo_view","select * from demo_view.passenger FOR SYSTEM_TIME FINISHED IN (10, 41) limit 2  ",
                val q1=QueryRequest("3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","eduejd01","SELECT\n" +
                        "\t'mark_01' AS metric,\n" +
                        "\tt2.short_name,\n" +
//                        "\tt.class_year_from,\n" +
                        "\tCAST(t.class_year_from AS VARCHAR),\n" +
                        "\tCAST(t3.period_num AS VARCHAR),\n" +
                        "\tt.subject_name,\n" +
                        "\tt.fact,\n" +
                        "\tNULL\n" +
                        "FROM\n" +
                        "\t(\n" +
                        "\tSELECT\n" +
                        "\t\tclass_year_from,\n" +
                        "\t\tschool_id,\n" +
                        "\t\tfact,\n" +
                        "\t\tsubject_name,\n" +
                        "\t\tclass_id,\n" +
                        "\t\tstudent_id,\n" +
                        "\t\tperiod_id\n" +
                        "\tFROM\n" +
                        "\t\teduejd01.pmarks) AS t\n" +
                        "INNER JOIN (\n" +
                        "\tSELECT\n" +
                        "\t\tid,\n" +
                        "\t\tsnils\n" +
                        "\tFROM\n" +
                        "\t\teduejd01.students\n" +
                        "\tWHERE\n" +
                        "\t\tsnils = 'snils') AS t1 ON\n" +
                        "\tt.student_id = t1.id\n" +
                        "INNER JOIN (\n" +
                        "\tSELECT\n" +
                        "\t\tid,\n" +
                        "\t\tshort_name\n" +
                        "\tFROM\n" +
                        "\t\teduejd01.schools) AS t2 ON\n" +
                        "\tt.school_id = t2.id\n" +
                        "INNER JOIN (\n" +
                        "\tSELECT\n" +
                        "\t\tperiod_num,\n" +
                        "\t\tstudent_id,\n" +
                        "\t\tclass_id,\n" +
                        "\t\tperiod_id\n" +
                        "\tFROM\n" +
                        "\t\teduejd01.classes_periods) AS t3 ON\n" +
                        "\tt.period_id = t3.period_id\n" +
                        "\tAND t.student_id = t3.student_id\n" +
                        "\tAND t.class_id = t3.class_id\n" +
                        "LIMIT 5000  ",
            mutableListOf(), mutableListOf(),false,0L)
            //listOf(q1).stream().forEach {q-> publishKafka(q, "demo_dev.query.rq")  }
            listOf(q1).stream().forEach {q-> publishKafka(q, "demo_dev.query.rq")  }
            println(" request sended ")

        }
        @JvmStatic
        public fun main1(args:Array<String>) {
            //val q1=QueryRequest("3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","demo_view","select * from demo_view.passenger FOR SYSTEM_TIME FINISHED IN (10, 41) limit 2  ",
            val q1=QueryRequest("3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","3424f5b3-e337-4d0d-a046-a1c491ab3310","foo","select * from cat1   ",
                mutableListOf(), mutableListOf(),false,0L)
            //listOf(q1).stream().forEach {q-> publishKafka(q, "demo_dev.query.rq")  }
            listOf(q1).stream().forEach {q-> publishKafka(q, "demo_dev.query.rq")  }
            println(" request sended ")

        }
        @JvmStatic
        fun publishKafka(u: QueryRequest, topic: String){
            val prod = getPproducer()
            prod?.send(ProducerRecord<String?, SpecificRecord?>(topic, u.getRequestId(), u))
            prod?.flush()
            prod?.close()


        }

        @JvmStatic
        open fun getPproducer(): Producer<String?, SpecificRecord?>? {
            val properties = Properties()

            //properties["metadata.max.idle.ms"] = "1000000000"
            //properties["metadata.max.age.ms"] = 1000_000_000 //5min
            //properties["connections.max.idle.ms"] = 1000_000_000 //9 min


            //properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "t5-one-04.ru-central1.internal:9092"
            properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "10.81.7.90:12541"
            //properties[ProducerConfig.BOOTSTRAP_SERVERS_CONFIG] = "p2-ads-04.load.local:9092"
            properties[ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG] = StringSerializer::class.java
            properties[ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG] = KafkaAvroSerializer::class.java
            return KafkaProducer(properties)
        }
    }
}