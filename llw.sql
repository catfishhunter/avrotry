create database dbllw

create readable external TABLE dbllw.passengerr
(
Code INT,
Id VARCHAR,
FirstName VARCHAR,
MiddleName VARCHAR,
LastName VARCHAR,
Birthday DATE,
Passport LINK,
primary key (code)
) distributed by (code)
LOCATION 'core:adb://dbllw.passenger'
OPTIONS ('auto.create.table.enable=false')



create writable external TABLE dbllw.passenger
(
Code INT,
Id VARCHAR,
FirstName VARCHAR,
MiddleName VARCHAR,
LastName VARCHAR,
Birthday DATE,
Passport LINK,
primary key (code)
) distributed by (code)
LOCATION 'core:adb://dbllw.passenger'
OPTIONS ('auto.create.table.enable=true')


drop UPLOAD EXTERNAL TABLE dbllw.passenger_ext
CREATE UPLOAD EXTERNAL TABLE dbllw.passenger_ext (
Code INT,
Id VARCHAR,
FirstName VARCHAR,
MiddleName VARCHAR,
LastName VARCHAR,
Birthday DATE,
Passport LINK
)
LOCATION  'kafka://t5-ads-01.ru-central1.internal:2181/query.tp'
FORMAT 'AVRO'
MESSAGE_LIMIT 1000
OPTIONS ('auto.create.sys_op.enable=false')

select count(1) from dbllw.passengerr

select * from dbllw.passengerr
delete from dbllw.passenger

insert into dbllw.passenger select * from dbllw.passenger_ext
insert into dbllw.passenger select nextval('seq'), e.* from dbllw.passenger_ext e

///two field
drop readable external TABLE dbllw.twor
create readable external TABLE dbllw.twor
(
id VARCHAR,
firstname VARCHAR--,
--primary key (id)
) --distributed by (id)
LOCATION 'core:adb://dbllw.two'
OPTIONS ('auto.create.table.enable=false')


drop writable external TABLE dbllw.two
create writable external TABLE dbllw.two
(
id VARCHAR,
firstname VARCHAR--,
--primary key (id)
) --distributed by (id)
LOCATION 'core:adb://dbllw.two'
OPTIONS ('auto.create.table.enable=true')


drop UPLOAD EXTERNAL TABLE dbllw.two_ext
CREATE UPLOAD EXTERNAL TABLE dbllw.two_ext (
id VARCHAR,
firstname VARCHAR
)
LOCATION  'kafka://t5-ads-01.ru-central1.internal:2181/query.tp'
FORMAT 'AVRO'
MESSAGE_LIMIT 1000
OPTIONS ('auto.create.sys_op.enable=false')

insert into dbllw.two select * from dbllw.two_ext 
select * from dbllw.twor 
delete from dbllw.two



///greenplum
drop EXTERNAL table dbllw.passenger_ext_pxf
CREATE READABLE EXTERNAL TABLE dbllw.passenger_ext_pxf (
Code INT,
Id VARCHAR,
FirstName VARCHAR,
MiddleName VARCHAR,
LastName VARCHAR,
Birthday DATE,
Passport VARCHAR
) LOCATION ('pxf://data.w7?PROFILE=kafka-greenplum-writer&KAFKA_BROKERS=t5-ads-01.ru-central1.internal:9092')
FORMAT 'CUSTOM' (FORMATTER='pxfwritable_import')

CREATE SERVER FDW_KAFKA_DEV
FOREIGN DATA WRAPPER kadb_fdw
OPTIONS (k_brokers 't5-ads-01.ru-central1.internal:9092');

drop FOREIGN TABLE dbllw.passenger_ext_fdw
CREATE FOREIGN TABLE dbllw.passenger_ext_fdw (
Code INT8,
Id VARCHAR,
FirstName VARCHAR,
MiddleName VARCHAR,
LastName VARCHAR,
Birthday DATE,
Passport VARCHAR
)
SERVER FDW_KAFKA_DEV
OPTIONS (
 format 'AVRO', k_topic 'data.w7', k_consumer_group 'TEST_GROUP',
 k_seg_batch '1000', k_timeout_ms '2000', k_initial_offset '0'
);

select * from dbllw.passenger_ext_fdw p join (select 1 a ) s on p.code=s.a
